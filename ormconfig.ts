// this file for cli typeorm
import { ConnectionOptions } from 'typeorm';
import { SnakeNamingStrategy } from 'typeorm-naming-strategies';

const ormconfig: ConnectionOptions = {
  type: 'postgres',
  host: 'localhost',
  port: +process.env.PG_PORT,
  username: process.env.PG_USER,
  password: process.env.PG_PASSWORD,
  database: process.env.PG_DB,
  synchronize: false,
  dropSchema: false,
  logging: true,
  entities: [__dirname + '/src/**/*.entity.ts', __dirname + '/dist/**/*.entity.js'],
  migrations: ['src/migrations/**/*.ts'],
  subscribers: ['subscriber/**/*.ts', 'dist/subscriber/**/.js'],
  cli: {
    entitiesDir: 'src',
    migrationsDir: 'src/migrations',
    subscribersDir: 'subscriber',
  },
  namingStrategy: new SnakeNamingStrategy(),
};

export default ormconfig;
