FROM node:14.17.3		

WORKDIR /server

COPY package*.json ./

RUN npm i

RUN mkdir log

# RUN npm i -g typeorm ts-node

COPY . . 		

CMD npm run start:dev