## To run the application in the terminal, enter:

`docker-compose up --build`

## Users

### Registration

`POST localhost:3000/worker/registration`

```bash
{
"worker_name": "Evgen",
"role": "user",
"password": "12323",
"email": "evgen@s-pro.io",
"position_id": 2
}
```

### Login

`POST localhost:3000/auth/login`

```bash
{
"worker_name": "Alex",
"password": "111",
}
```

### Get all users

`GET localhost:3000/worker`

Headers.
Authorization: Bearer eyJhbGciO.....

### Get one user

`GET localhost:3000/worker/3`

Headers.
Authorization: Bearer eyJhbGciO.....

### User modification

`PATCH localhost:3000/worker/3`

```bash
{
"worker_name": "Alexey",
"role": "admin",
"email": "alexey@s-pro.io",
"position_id": 1
}
```

Headers.
Authorization: Bearer eyJhbGciO.....

### Set avatar for user

`POST localhost:3000/worker/avatar/<workerId>`

form-data:
"file" anakin.jpg

Headers.
Authorization: Bearer eyJhbGciO.....

### Delete user

`localhost:3000/worker/1`
Headers.
Authorization: Bearer eyJhbGciO.....

###

` PATCH localhost:3000/worker/setTask/<workerId>`

```bash
{
    "tasksId": [2, 3, 4, 5, 6, 7]
}
```

Headers.
Authorization: Bearer eyJhbGciO.....

## Tasks

### Get all tasks

`GET localhost:3000/task`
Headers.
Authorization: Bearer eyJhbGciO.....

### Get one task

`GET localhost:3000/task/1`
Headers.
Authorization: Bearer eyJhbGciO.....

### Task modification

`PATCH localhost:3000/task/3`

```bash
{
    "status": "done"
}
```

Headers.
Authorization: Bearer eyJhbGciO.....

### Delete task

`DELETE localhost:3000/task/2`
Headers.
Authorization: Bearer eyJhbGciO.....

### Set Workers for a Task

`PATCH localhost:3000/task/setWorker/<taskId>`

```bash
{
    "workersId": [2, 3, 4]
}
```

Headers.
Authorization: Bearer eyJhbGciO.....

## Positions

### Get all positions

`GET localhost:3000/position`
Headers.
Authorization: Bearer eyJhbGciO.....

### Get one position

`GET localhost:3000/position/3`
Headers.
Authorization: Bearer eyJhbGciO.....

### Create new position

`POST localhost:3000/position`

```bash
{
    "title": "QA engineer"
}
```

Headers.
Authorization: Bearer eyJhbGciO.....

### Update position

`PATCH localhost:3000/position/3`

```bash
{
    "title": "QA Eng."
}
```

Headers.
Authorization: Bearer eyJhbGciO.....

### Delete position

`DELETE localhost:3000/position/4`
Headers.
Authorization: Bearer eyJhbGciO.....

## Get avatar

In browser:
`http://localhost:3000/anakin-36c4.jpg`
