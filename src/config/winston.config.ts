import * as winston from 'winston';
import * as path from 'path';

const errorFilter = winston.format((info, opts) => {
  return info.level === 'error' ? info : false;
});

const infoFilter = winston.format((info, opts) => {
  return info.level === 'http' ? info : false;
});

export const winstonConfig = {
  exitOnError: false,
  format: winston.format.combine(
    winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
    winston.format.json(),
    // winston.format.splat(),
    // winston.format.label({ label: 'Smartedu' }),
  ),
  transports: [
    new winston.transports.Console({
      level: 'debug',
      format: winston.format.combine(
        winston.format.colorize({ all: true }),
        winston.format.prettyPrint(),
        winston.format.printf((msg) => {
          return `${msg.timestamp} [${msg.level}] - ${msg.message}`;
        }),
      ),
    }),
    new winston.transports.File({
      dirname: path.join(__dirname, './../../../src/log/error/'),
      filename: 'error.log',
      level: 'error',
      format: winston.format.combine(errorFilter(), winston.format.json()),
    }),
    new winston.transports.File({
      dirname: path.join(__dirname, './../../../src/log/info/'),
      filename: 'info.log',
      level: 'info',
    }),
    new winston.transports.File({
      dirname: path.join(__dirname, './../../../src/log/access/'),
      filename: 'access.log',
      level: 'http',
      format: winston.format.combine(infoFilter(), winston.format.json()),
      maxsize: 100000,
    }),
  ],
};
