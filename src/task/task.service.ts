import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Worker } from '../worker/entities/worker.entity';
import { getConnection, getRepository, Repository } from 'typeorm';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { Task } from './entities/task.entity';
import { SetWorkerForTask } from './dto/setWorkerForTask.dto';

@Injectable()
export class TaskService {
  constructor(
    @InjectRepository(Task)
    private taskRepository: Repository<Task>,

    @InjectRepository(Worker) // ???
    private workerRepository: Repository<Worker>,
  ) {}

  async create(createTaskDto: CreateTaskDto, worker_id: number): Promise<Task> {
    try {
      const newTask = await this.taskRepository.create(createTaskDto);
      const savedTask = await this.taskRepository.save(newTask);

      // console.log('create task ===>>>', worker_id);
      // const currentWorker = await this.workerRepository.findOne(worker_id, {
      //   relations: ['tasks'],
      // });
      // currentWorker.tasks.push(savedTask);
      // await this.workerRepository.save(currentWorker);
      // return savedTask;
      await getConnection()
        .createQueryBuilder()
        .relation(Worker, 'tasks')
        .of(worker_id)
        .add(savedTask);
      return savedTask;
    } catch (err) {
      throw new Error(err.message);
    }
  }

  findAll(): Promise<Task[]> {
    return this.taskRepository.find();
  }

  async findOne(id: number): Promise<Task> {
    try {
      const task = await this.taskRepository.findOne(id, { relations: ['workers'] });
      //  const task = await this.taskRepository.findOne(id);
      return task;
    } catch (err) {
      throw new Error(err.message);
    }
  }

  async setWorkerForTask(id: number, workersIdDto: SetWorkerForTask) {
    try {
      // const task = await this.taskRepository.findOne(id);

      const actualRelationships = await getConnection()
        .createQueryBuilder()
        .relation(Task, 'workers')
        .of(id)
        .loadMany();

      // const newWorkerArr: number[] = [...workersIdDto.workersId];

      // actualRelationships.forEach((item) => {
      //   newWorkerArr.push(item.id);
      // });

      await getConnection()
        .createQueryBuilder()
        .relation(Task, 'workers')
        .of(id)
        .addAndRemove(workersIdDto.workersId, actualRelationships);

      // await getConnection()
      //   .createQueryBuilder()
      //   .relation(Task, 'workers')
      //   .of(id)
      //   .remove(workersIdDto.workersId); // ???
      // const result = await getConnection()
      //   .createQueryBuilder()
      //   .relation(Task, 'workers')
      //   .of(id)
      //   .add(workersIdDto.workersId);
      return workersIdDto.workersId;
    } catch (err) {
      throw new Error(err.message);
    }
  }

  async update(id: number, updateTaskDto: UpdateTaskDto): Promise<Task> {
    try {
      // var1
      // const updatedTask = await this.taskRepository.create(updateTaskDto);
      // return this.taskRepository.update(id, updatedTask);
      // var2
      // const updatedTask = await this.taskRepository.create(updateTaskDto);
      // const task = await getRepository(Task)
      //   .createQueryBuilder('task')
      //   .update(Task)
      //   .set(updatedTask)
      //   .where('id = :id', { id })
      //   .execute();
      // return task
      // var3
      const oldTask = await this.taskRepository.findOne(id);
      if (!oldTask) throw new Error('This task is not found');
      const newTask = await this.taskRepository.save({
        ...oldTask,
        ...updateTaskDto,
      });
      return newTask;
    } catch (err) {
      throw new Error(err.message);
    }
  }

  async remove(id: number) {
    return this.taskRepository.delete(id);
  }
}
