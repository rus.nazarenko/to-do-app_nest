import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  HttpException,
  HttpStatus,
  Request,
} from '@nestjs/common';
import { TaskService } from './task.service';
import { CreateTaskDto } from './dto/create-task.dto';
import { UpdateTaskDto } from './dto/update-task.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { SetWorkerForTask } from './dto/setWorkerForTask.dto';
import { Task } from './entities/task.entity';

@Controller('task')
export class TaskController {
  constructor(private readonly taskService: TaskService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Body() createTaskDto: CreateTaskDto, @Request() req): Promise<Task> {
    return this.taskService.create(createTaskDto, req.user.userId);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  findAll(): Promise<Task[]> {
    return this.taskService.findAll();
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Task> {
    try {
      const task = await this.taskService.findOne(+id);
      if (task === undefined) {
        throw new HttpException('This task is not found ', HttpStatus.NOT_FOUND);
      }
      return task;
    } catch (err) {
      throw new Error(err.message);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateTaskDto: UpdateTaskDto): Promise<Task> {
    try {
      const updatedTask = await this.taskService.update(+id, updateTaskDto);
      // if (updatedTask.affected) return 'successful';
      // else throw new HttpException('This task is not found ', HttpStatus.NOT_FOUND);
      return updatedTask;
    } catch (err) {
      throw new Error(err.message);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Patch('/setWorker/:id')
  async setWorkerForTaskController(
    @Param('id') id: string,
    @Body() workersIdDto: SetWorkerForTask,
  ) {
    try {
      const newWorkerArr = await this.taskService.setWorkerForTask(+id, workersIdDto);
      return newWorkerArr;
    } catch (err) {
      throw new Error(err.message);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  async remove(@Param('id') id: string) {
    try {
      const result = await this.taskService.remove(+id);
      if (result.affected) return 'successful';
      else throw new HttpException('This task is not found ', HttpStatus.NOT_FOUND);
    } catch (err) {
      throw new Error(err.message);
    }
  }
}
