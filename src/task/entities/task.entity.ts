import { Worker } from 'src/worker/entities/worker.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';

export enum TaskStatus {
  NEW = 'new',
  IN_PROGRESS = 'in progress',
  DONE = 'done',
}

@Entity()
export class Task {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  task_name: string;

  @Column()
  complexity: number;

  @Column({ type: 'enum', enum: TaskStatus, default: TaskStatus.NEW })
  status: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @ManyToMany(() => Worker, (worker) => worker.tasks)
  workers: Worker[];
}
