import { Module } from '@nestjs/common';
import { TaskService } from './task.service';
import { TaskController } from './task.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Task } from './entities/task.entity';
import { Worker } from 'src/worker/entities/worker.entity';

@Module({
  controllers: [TaskController],
  providers: [TaskService],
  imports: [TypeOrmModule.forFeature([Task]), TypeOrmModule.forFeature([Worker])], // TypeOrmModule.forFeature([Worker]) ???
  // exports: []
})
export class TaskModule {}
