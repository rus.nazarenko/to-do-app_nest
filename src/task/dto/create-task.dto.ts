import {
  IsAlpha,
  IsByteLength,
  IsIn,
  IsNumber,
  IsOptional,
  IsString,
  Length,
  Max,
  Min,
} from 'class-validator';
import { TaskStatus } from '../entities/task.entity';

export class CreateTaskDto {
  @IsString()
  // @IsByteLength(3, 20)
  @Length(3, 20)
  @IsAlpha()
  task_name: string;

  @IsNumber()
  @Min(1)
  @Max(10)
  complexity: number;

  @IsOptional()
  @IsIn(['new', 'in progress', 'done'])
  status?: TaskStatus;
  // worker_id?: string;
}
