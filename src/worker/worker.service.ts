import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { getConnection, getRepository, Repository } from 'typeorm';
import { CreateWorkerDto } from './dto/create-worker.dto';
import { UpdateWorkerDto } from './dto/update-worker.dto';
import { SetTaskForWorker } from './dto/setTaskForWorker.dto';
import { Worker } from './entities/worker.entity';
import { Position } from 'src/position/entities/position.entity';

@Injectable()
export class WorkerService {
  constructor(
    @InjectRepository(Worker)
    private workerRepository: Repository<Worker>,
  ) {}

  async create(createWorkerDto: CreateWorkerDto): Promise<Worker> {
    // console.log('create worker ====>>>', createWorkerDto);
    const newWorker = await this.workerRepository.create(createWorkerDto);

    const positionRepository = getRepository(Position);
    const assignedPosition = await positionRepository.findOne({ id: createWorkerDto.position_id });

    if (assignedPosition) newWorker.position = assignedPosition;
    // console.log('save worker ====>>>', newWorker);
    const savedWorker = await this.workerRepository.save(newWorker);
    return savedWorker;
  }

  findAll(): Promise<Worker[]> {
    return this.workerRepository.find();
  }

  async findOneByName(worker_name: string): Promise<Worker> {
    const worker = await this.workerRepository.find({ worker_name });
    return worker[0];
  }

  async findOne(id: number): Promise<Worker> {
    const worker = await this.workerRepository.findOne(id, { relations: ['tasks', 'position'] });
    // const worker = await getRepository(Worker)
    //   .createQueryBuilder('worker')
    //   .leftJoinAndSelect('worker.tasks', 'task')
    //   .where('worker.id = :id', { id })
    //   .getOne();
    return worker;
  }

  async setTaskForWorker(id: number, tasksIdDto: SetTaskForWorker) {
    const actualRelationships = await getConnection()
      .createQueryBuilder()
      .relation(Worker, 'tasks')
      .of(id)
      .loadMany();

    await getConnection()
      .createQueryBuilder()
      .relation(Worker, 'tasks')
      .of(id)
      .addAndRemove(tasksIdDto.tasksId, actualRelationships);

    return tasksIdDto.tasksId;
  }

  async setAvatar(id: number, avatar: string) {
    const currentWorker = await this.workerRepository.findOne(id);
    if (!currentWorker) throw new Error('This worker is not found');
    const updatedWorker = { ...currentWorker, avatar };

    const savedWorker = await this.workerRepository.save({ ...updatedWorker });
    return savedWorker;
  }

  async update(id: number, updateWorkerDto: UpdateWorkerDto): Promise<Worker> {
    try {
      const oldWorker = await this.workerRepository.findOne(id);
      if (!oldWorker) throw new Error('This worker is not found');
      const newWorker = { ...oldWorker, ...updateWorkerDto };

      const positionRepository = getRepository(Position);
      const assignedPosition = await positionRepository.findOne(updateWorkerDto.position_id);
      newWorker.position = assignedPosition;
      const savedWorker = await this.workerRepository.save(newWorker);

      return savedWorker;
    } catch (err) {
      throw new Error(err.message);
    }
  }

  remove(id: number) {
    return this.workerRepository.delete(id);
  }
}
