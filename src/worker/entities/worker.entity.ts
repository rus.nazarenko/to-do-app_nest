import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  BeforeInsert,
  BeforeUpdate,
  ManyToMany,
  JoinTable,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
} from 'typeorm';
import * as bcrypt from 'bcrypt';
import { Task } from 'src/task/entities/task.entity';
import { Position } from 'src/position/entities/position.entity';

export enum WorkerRole {
  ADMIN = 'admin',
  USER = 'user',
}

@Entity()
export class Worker {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  worker_name: string;

  @Column({
    type: 'enum',
    enum: WorkerRole,
    default: WorkerRole.USER,
  })
  role: WorkerRole;

  @BeforeInsert()
  @BeforeUpdate()
  async hashPassword() {
    this.password = await bcrypt.hash(this.password, 10);
  }

  @Column()
  password: string;

  @Column()
  avatar: string;

  @Column({ unique: true, nullable: false })
  email: string;

  @ManyToOne(() => Position, (position) => position.workers)
  position: Position;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @ManyToMany(() => Task, (task) => task.workers)
  @JoinTable()
  tasks: Task[];
}
