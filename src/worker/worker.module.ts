import { Module } from '@nestjs/common';
import { WorkerService } from './worker.service';
import { WorkerController } from './worker.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Worker } from './entities/worker.entity';

@Module({
  controllers: [WorkerController],
  providers: [WorkerService],
  imports: [TypeOrmModule.forFeature([Worker])],
  exports: [WorkerService],
})
export class WorkerModule {}
