import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  HttpException,
  HttpStatus,
  UseGuards,
  UseInterceptors,
  UploadedFile,
} from '@nestjs/common';
import { WorkerService } from './worker.service';
import { CreateWorkerDto } from './dto/create-worker.dto';
import { UpdateWorkerDto } from './dto/update-worker.dto';
import { SetTaskForWorker } from './dto/setTaskForWorker.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { Worker } from './entities/worker.entity';
import { FileInterceptor } from '@nestjs/platform-express';
import { multerOptions } from 'src/config/multer.config';

@Controller('worker')
export class WorkerController {
  constructor(private readonly workerService: WorkerService) {}

  @Post('registration')
  create(@Body() createWorkerDto: CreateWorkerDto) {
    return this.workerService.create(createWorkerDto);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  findAll(): Promise<Worker[]> {
    try {
      return this.workerService.findAll();
    } catch (err) {
      throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Worker> {
    try {
      const worker = await this.workerService.findOne(+id);
      if (worker === undefined) {
        throw new HttpException('This user is not found ', HttpStatus.NOT_FOUND);
      }
      return worker;
    } catch (err) {
      throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Patch('/setTask/:id')
  async setTaskForWorkerController(@Param('id') id: string, @Body() tasksIdDto: SetTaskForWorker) {
    try {
      const newTaskArr = await this.workerService.setTaskForWorker(+id, tasksIdDto);
      return newTaskArr;
    } catch (err) {
      throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
    }
  }

  // @UseGuards(JwtAuthGuard)
  // @Patch('/setPosition/:id')
  // async setPositionForWorkerController(
  //   @Param('id') id: string,
  //   @Body() tasksIdDto: SetTaskForWorker,
  // ) {
  //   const newTaskArr = await this.workerService.setTaskForWorker(+id, tasksIdDto);
  //   return newTaskArr;
  // }

  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  async update(@Param('id') id: string, @Body() updateWorkerDto: UpdateWorkerDto): Promise<Worker> {
    try {
      const updatedWorker = await this.workerService.update(+id, updateWorkerDto);
      // if (updatedWorker.affected) return 'successful';
      // else throw new HttpException('This user is not found', HttpStatus.NOT_FOUND);
      console.log('update Worker Controller ===>>>', updatedWorker);
      return updatedWorker;
    } catch (err) {
      throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Post('/avatar/:id')
  @UseInterceptors(FileInterceptor('file', multerOptions))
  async setAvatar(@Param('id') id: string, @UploadedFile() file: Express.Multer.File) {
    try {
      const avatar: string = file.filename;
      const result = await this.workerService.setAvatar(+id, avatar);
      return result;
    } catch (err) {
      throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  async remove(@Param('id') id: string) {
    try {
      const result = await this.workerService.remove(+id);
      if (result.affected) return 'successful';
      else throw new HttpException('This user is not found', HttpStatus.NOT_FOUND);
    } catch (err) {
      throw new HttpException(err.message, HttpStatus.BAD_REQUEST);
    }
  }
}
