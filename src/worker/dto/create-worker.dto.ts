import {
  IsAlphanumeric,
  IsByteLength,
  IsEmail,
  IsIn,
  IsNumber,
  IsOptional,
  IsString,
  Length,
} from 'class-validator';
import { WorkerRole } from '../entities/worker.entity';

export class CreateWorkerDto {
  @IsString()
  // @IsByteLength(3, 12)
  @Length(3, 12)
  @IsAlphanumeric()
  worker_name: string;

  @IsOptional()
  @IsIn(['user', 'admin'])
  role?: WorkerRole;

  @IsString()
  // @IsByteLength(6)
  @Length(6)
  @IsAlphanumeric()
  password: string;

  @IsEmail()
  email: string;

  @IsOptional()
  @IsNumber()
  position_id?: number;
}
