import { IsByteLength, IsEmail, IsIn, IsNumber, IsOptional, IsString } from 'class-validator';
import { PartialType } from '@nestjs/mapped-types';
import { CreateWorkerDto } from './create-worker.dto';
import { WorkerRole } from '../entities/worker.entity';

export class UpdateWorkerDto extends PartialType(CreateWorkerDto) {
  // @IsOptional()
  // @IsString()
  // @IsByteLength(3, 12)
  // worker_name?: string;
  // @IsOptional()
  // @IsIn(['user', 'admin'])
  // role?: WorkerRole;
  // @IsOptional()
  // @IsString()
  // @IsByteLength(6)
  // password?: string;
  // @IsOptional()
  // @IsEmail()
  // email?: string;
  // @IsOptional()
  // @IsNumber()
  // position_id?: number;
}
