import { IsArray } from 'class-validator';

export class SetTaskForWorker {
  @IsArray()
  tasksId: number[];
}
