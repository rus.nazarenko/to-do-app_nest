import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from './auth.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    super();
  }

  async validate(worker_name: string, password: string): Promise<any> {
    const worker = await this.authService.validateWorker(worker_name, password);
    if (!worker) {
      throw new UnauthorizedException();
    }
    return worker;
  }
}
