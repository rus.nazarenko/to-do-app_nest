import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { WorkerService } from 'src/worker/worker.service';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(
    private readonly workerService: WorkerService,
    private readonly jwtService: JwtService,
  ) {}

  async validateWorker(worker_name: string, pass: string): Promise<any> {
    const worker = await this.workerService.findOneByName(worker_name);
    const resultBcrypt = await bcrypt.compare(pass, worker.password);

    if (resultBcrypt) {
      const { password, ...result } = worker;
      return result;
    }
    return null;
  }

  async login(user: any) {
    const payload = { username: user.worker_name, sub: user.id };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
