import { MigrationInterface, QueryRunner } from 'typeorm';

export class createTables1643098876343 implements MigrationInterface {
  name = 'createTables1643098876343';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TYPE "public"."task_status_enum" AS ENUM('new', 'in progress', 'done')`,
    );
    await queryRunner.query(
      `CREATE TABLE "task" (
          "id" SERIAL NOT NULL, 
          "task_name" character varying NOT NULL, 
          "complexity" smallint NOT NULL, 
          "status" "public"."task_status_enum" NOT NULL DEFAULT 'new', 
          "created_at" TIMESTAMP NOT NULL DEFAULT now(), 
          "updated_at" TIMESTAMP NOT NULL DEFAULT now(), 
          CONSTRAINT "PK_fb213f79ee45060ba925ecd576e" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(`CREATE TYPE "public"."worker_role_enum" AS ENUM('admin', 'user')`);
    await queryRunner.query(
      `CREATE TABLE "worker" (
          "id" SERIAL NOT NULL, 
          "worker_name" character varying NOT NULL, 
          "role" "public"."worker_role_enum" NOT NULL DEFAULT 'user', 
          "password" character varying NOT NULL, 
          "email" character varying NOT NULL,
          "avatar" character varying,
          "position_id" integer, 
          "created_at" TIMESTAMP NOT NULL DEFAULT now(), 
          "updated_at" TIMESTAMP NOT NULL DEFAULT now(),
          CONSTRAINT "UQ_13679fa10b68bb29e4303ca1c91" UNIQUE ("email"), 
          CONSTRAINT "PK_dc8175fa0e34ce7a39e4ec73b94" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "position" (
          "id" SERIAL NOT NULL, 
          "title" character varying NOT NULL, 
          "created_at" TIMESTAMP NOT NULL DEFAULT now(), 
          "updated_at" TIMESTAMP NOT NULL DEFAULT now(), 
          CONSTRAINT "PK_b7f483581562b4dc62ae1a5b7e2" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "worker_tasks_task" (
          "worker_id" integer NOT NULL, 
          "task_id" integer NOT NULL, 
          CONSTRAINT "PK_57564c5700b14838850fd942f7e" 
          PRIMARY KEY ("worker_id", "task_id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_8b26c4d9987e95f9917b08ef7b" ON "worker_tasks_task" ("worker_id") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_fa0ca45ae33d406b88c32e221e" ON "worker_tasks_task" ("task_id") `,
    );
    await queryRunner.query(
      `ALTER TABLE "worker" ADD CONSTRAINT "FK_3aaeca3fdffa554e2226d6e4a67" FOREIGN KEY ("position_id") REFERENCES "position"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE "worker_tasks_task" ADD CONSTRAINT "FK_8b26c4d9987e95f9917b08ef7ba" FOREIGN KEY ("worker_id") REFERENCES "worker"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "worker_tasks_task" ADD CONSTRAINT "FK_fa0ca45ae33d406b88c32e221ec" FOREIGN KEY ("task_id") REFERENCES "task"("id") ON DELETE CASCADE ON UPDATE CASCADE`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "worker_tasks_task" DROP CONSTRAINT "FK_fa0ca45ae33d406b88c32e221ec"`,
    );
    await queryRunner.query(
      `ALTER TABLE "worker_tasks_task" DROP CONSTRAINT "FK_8b26c4d9987e95f9917b08ef7ba"`,
    );
    await queryRunner.query(
      `ALTER TABLE "worker" DROP CONSTRAINT "FK_3aaeca3fdffa554e2226d6e4a67"`,
    );
    await queryRunner.query(`DROP INDEX "public"."IDX_fa0ca45ae33d406b88c32e221e"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_8b26c4d9987e95f9917b08ef7b"`);
    await queryRunner.query(`DROP TABLE "worker_tasks_task"`);
    await queryRunner.query(`DROP TABLE "position"`);
    await queryRunner.query(`DROP TABLE "worker"`);
    await queryRunner.query(`DROP TYPE "public"."worker_role_enum"`);
    await queryRunner.query(`DROP TABLE "task"`);
    await queryRunner.query(`DROP TYPE "public"."task_status_enum"`);
  }
}
