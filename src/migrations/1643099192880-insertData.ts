import { MigrationInterface, QueryRunner } from 'typeorm';
import * as bcrypt from 'bcrypt';

const hash = bcrypt.hashSync('111', 10);

export class insertData1643099192880 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    console.log('migration Add data on Table ===>>>');

    await queryRunner.query(
      `INSERT INTO position (title)
      VALUES('developer'),
      ('sys. administrator'),
      ('tech lead')`,
    );

    await queryRunner.query(
      `INSERT INTO worker (worker_name, role, password, email, position_id)
      VALUES ('Ruslan', 'user', '${hash}', 'rus@mail.com', 1),
      ('Alex', 'user', '${hash}', 'alex@mail.com', 2),
      ('Dima', 'user', '${hash}', 'dima@mail.com', 3)`,
    );

    await queryRunner.query(
      `INSERT INTO task (task_name, complexity)
      VALUES('education', 5),
      ('reding', 10),
      ('coding', 10)`,
    );

    await queryRunner.query(
      `INSERT INTO worker_tasks_task (worker_id, task_id)
      VALUES(1, 1),
      (1, 3),
      (2, 1)`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DELETE FROM worker`);
    await queryRunner.query(`DELETE FROM task`);
    await queryRunner.query(`DELETE FROM worker_tasks_task`);
  }
}
