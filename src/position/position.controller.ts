import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { PositionService } from './position.service';
import { CreatePositionDto } from './dto/create-position.dto';
import { UpdatePositionDto } from './dto/update-position.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { Position } from './entities/position.entity';

@Controller('position')
export class PositionController {
  constructor(private readonly positionService: PositionService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Body() createPositionDto: CreatePositionDto): Promise<Position> {
    return this.positionService.create(createPositionDto);
  }

  @UseGuards(JwtAuthGuard)
  @Get()
  findAll(): Promise<Position[]> {
    return this.positionService.findAll();
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  findOne(@Param('id') id: string): Promise<Position> {
    return this.positionService.findOne(+id);
  }

  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updatePositionDto: UpdatePositionDto,
  ): Promise<Position> {
    try {
      const savedPosition = await this.positionService.update(+id, updatePositionDto);
      return savedPosition;
    } catch (err) {
      console.log('error controller ===>>>', err);
      throw new HttpException(err.message, HttpStatus.NOT_FOUND);
    }
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  async remove(@Param('id') id: string) {
    const result = await this.positionService.remove(+id);

    if (result.affected) return 'successful';
    else throw new HttpException('This position is not found', HttpStatus.NOT_FOUND);
  }
}
