import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreatePositionDto } from './dto/create-position.dto';
import { UpdatePositionDto } from './dto/update-position.dto';
import { Position } from './entities/position.entity';

@Injectable()
export class PositionService {
  constructor(
    @InjectRepository(Position)
    private positionRepository: Repository<Position>,
  ) {}

  async create(createPositionDto: CreatePositionDto): Promise<Position> {
    const newPosition = await this.positionRepository.create(createPositionDto);
    const savedPosition = await this.positionRepository.save(newPosition);
    return savedPosition;
  }

  findAll(): Promise<Position[]> {
    return this.positionRepository.find();
  }

  findOne(id: number): Promise<Position> {
    return this.positionRepository.findOne(id);
  }

  async update(id: number, updatePositionDto: UpdatePositionDto): Promise<Position> {
    try {
      const oldPosition = await this.positionRepository.findOne(id);
      if (!oldPosition) throw new Error('This position is not found');
      const newPosition = { ...oldPosition, ...updatePositionDto };
      const savedPosition = await this.positionRepository.save(newPosition);
      return savedPosition;
    } catch (err) {
      throw new Error(err.message);
    }
  }

  remove(id: number) {
    return this.positionRepository.delete(id);
  }
}
