import { IsAlpha, IsAlphanumeric, IsByteLength, IsString, Length, Matches } from 'class-validator';

export class CreatePositionDto {
  @IsString()
  // @IsByteLength(3, 20)
  @Length(3, 20)
  // @IsAlpha()
  // @IsAlphanumeric()
  title: string;
}
