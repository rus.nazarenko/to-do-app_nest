import { Injectable, NestInterceptor, ExecutionContext, CallHandler, Inject } from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { WINSTON_MODULE_PROVIDER } from 'nest-winston';
import { Logger } from 'winston';

@Injectable()
export class LoggingInterceptor implements NestInterceptor {
  constructor(@Inject(WINSTON_MODULE_PROVIDER) private readonly logger: Logger) {}

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const ctx = context.switchToHttp();
    const request = ctx.getRequest<Request>();
    const response = ctx.getResponse<Response>();
    new Error('qwert');
    this.logger.http({
      method: request.method,
      agent: request.headers['user-agent'],
      host: request.headers['host'],
      status: response['statusCode'],
      url: request.url,
    });
    // const now = Date.now();
    // return next.handle().pipe(tap(() => console.log(`After... ${Date.now() - now}ms`)));
    return next.handle().pipe(tap());
  }
}
