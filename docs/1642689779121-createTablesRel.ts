import { MigrationInterface, QueryRunner } from 'typeorm';

export class createTablesRel1642689779121 implements MigrationInterface {
  name = 'createTablesRel1642689779121';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "worker" (
          "id" SERIAL NOT NULL, 
          "worker_name" character varying NOT NULL, 
          "role" character varying DEFAULT 'user',
          "password" character varying NOT NULL, 
          "email" character varying NOT NULL UNIQUE, 
          CONSTRAINT "PK_dc8175fa0e34ce7a39e4ec73b94" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "task" (
          "id" SERIAL NOT NULL, 
          "task_name" character varying NOT NULL, 
          "complexity" character varying NOT NULL, 
          "status" character varying DEFAULT 'new', 
          CONSTRAINT "PK_fb213f79ee45060ba925ecd576e" PRIMARY KEY ("id"))`,
    );
    await queryRunner.query(
      `CREATE TABLE "worker_tasks_task" (
          "worker_id" integer NOT NULL, 
          "task_id" integer NOT NULL, 
          CONSTRAINT "PK_57564c5700b14838850fd942f7e" PRIMARY KEY ("worker_id", "task_id"))`,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_8b26c4d9987e95f9917b08ef7b" ON "worker_tasks_task" ("worker_id") `,
    );
    await queryRunner.query(
      `CREATE INDEX "IDX_fa0ca45ae33d406b88c32e221e" ON "worker_tasks_task" ("task_id") `,
    );
    await queryRunner.query(
      `ALTER TABLE "worker_tasks_task" 
      ADD CONSTRAINT "FK_8b26c4d9987e95f9917b08ef7ba" 
      FOREIGN KEY ("worker_id") 
      REFERENCES "worker"("id") 
      ON DELETE CASCADE 
      ON UPDATE CASCADE`,
    );
    await queryRunner.query(
      `ALTER TABLE "worker_tasks_task" 
      ADD CONSTRAINT "FK_fa0ca45ae33d406b88c32e221ec" 
      FOREIGN KEY ("task_id") 
      REFERENCES "task"("id") 
      ON DELETE CASCADE 
      ON UPDATE CASCADE`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "worker_tasks_task" DROP CONSTRAINT "FK_fa0ca45ae33d406b88c32e221ec"`,
    );
    await queryRunner.query(
      `ALTER TABLE "worker_tasks_task" DROP CONSTRAINT "FK_8b26c4d9987e95f9917b08ef7ba"`,
    );
    await queryRunner.query(`DROP INDEX "public"."IDX_fa0ca45ae33d406b88c32e221e"`);
    await queryRunner.query(`DROP INDEX "public"."IDX_8b26c4d9987e95f9917b08ef7b"`);
    await queryRunner.query(`DROP TABLE "worker_tasks_task"`);
    await queryRunner.query(`DROP TABLE "task"`);
    await queryRunner.query(`DROP TABLE "worker"`);
  }
}
