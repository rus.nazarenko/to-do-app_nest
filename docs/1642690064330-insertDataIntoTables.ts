import { MigrationInterface, QueryRunner } from 'typeorm';
import * as bcrypt from 'bcrypt';

const hash = bcrypt.hashSync('111', 10);

export class insertDataIntoTables1642690064330 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    console.log('migration Add data on Table ===>>>');
    await queryRunner.query(
      `INSERT INTO worker (worker_name, role, password, email)
        VALUES ('Ruslan', 'user', '${hash}', 'rus@mail.com'),
        ('Alex', 'user', '${hash}', 'alex@mail.com'),
        ('Dima', 'user', '${hash}', 'dima@mail.com')`,
    );

    await queryRunner.query(
      `INSERT INTO task (task_name, complexity)
        VALUES('education', 5),
        ('reding', 10),
        ('coding', 10)`,
    );

    await queryRunner.query(
      `INSERT INTO worker_tasks_task (worker_id, task_id)
        VALUES(1, 1),
        (1, 3),
        (2, 1)`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DELETE FROM worker`);
    await queryRunner.query(`DELETE FROM task`);
    await queryRunner.query(`DELETE FROM worker_tasks_task`);
  }
}
